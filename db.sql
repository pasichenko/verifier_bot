create table admins
(
    user_id    bigint                        not null,
    first_name text,
    last_name  text,
    username   text,
    is_active  boolean default false         not null,
    role       text    default 'admin'::text not null
);

alter table admins
    owner to qnduqp;

create table groups
(
    group_id bigint not null,
    name     text   not null
);

alter table groups
    owner to qnduqp;

create unique index groups_group_id_uindex
    on groups (group_id);

create table users
(
    user_id     bigint                not null,
    first_name  text,
    last_name   text,
    username    text,
    is_verified boolean default false not null,
    comment     text,
    sys_comment text,
    is_banned   boolean default false not null
);

alter table users
    owner to qnduqp;

create unique index users_user_id_uindex
    on users (user_id);

