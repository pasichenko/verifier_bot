import os

from peewee import *
from playhouse.shortcuts import ReconnectMixin


class ReconnectPostgresqlDatabase(ReconnectMixin, PostgresqlDatabase):
    pass


db_user = os.environ.get('db_user')
db_password = os.environ.get('db_password')
db_host = os.environ.get('db_host')
db_port = int(os.environ.get('db_port'))
db_name = os.environ.get('db_name')
db_schema = os.environ.get('db_schema')

dbhandle = ReconnectPostgresqlDatabase(db_name, user=db_user, password=db_password,
                                       host=db_host, port=db_port)


class BaseModel(Model):
    class Meta:
        database = dbhandle
        schema = db_schema
        autorollback = True


class Admins(BaseModel):
    user_id = PrimaryKeyField(column_name="user_id", null=False)
    first_name = TextField(column_name="first_name")
    last_name = TextField(column_name="last_name")
    username = TextField(column_name="username")
    is_active = BooleanField(column_name="is_active", null=False)
    role = TextField(column_name="role", null=False)

    class Meta:
        table_name = 'admins'


class Groups(BaseModel):
    group_id = PrimaryKeyField(column_name="group_id", null=False)
    name = TextField(column_name="name", null=False)

    class Meta:
        table_name = 'groups'


class Users(BaseModel):
    user_id = PrimaryKeyField(column_name="user_id", null=False)
    first_name = TextField(column_name="first_name")
    last_name = TextField(column_name="last_name")
    username = TextField(column_name="username")
    is_verified = BooleanField(column_name="is_verified", null=False)
    is_banned = BooleanField(column_name="is_banned", null=False)
    comment = TextField(column_name="comment")
    sys_comment = TextField(column_name="sys_comment")

    class Meta:
        table_name = 'users'
