from flask import Flask

from db.schema import dbhandle

app = Flask(__name__)


@app.before_request
def _db_connect():
    dbhandle.connect()


@app.teardown_request
def _db_close(exc):
    if not dbhandle.is_closed():
        dbhandle.close()


@app.teardown_appcontext
def close_database(error):
    dbhandle.close()