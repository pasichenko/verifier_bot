import os
from datetime import datetime

import socks
from telethon import TelegramClient, events, Button

import const
from db.schema import *
import logging
conversation_state = {}

api_id = int(os.environ.get('api_id'))
api_hash = os.environ.get('api_hash')
bot_token = os.environ.get('bot_token')
proxy_enable = os.environ.get('proxy_enable', 'False').lower() in ('true')
proxy_ip = os.environ.get('proxy_ip')
proxy_port = int(os.environ.get('proxy_port'))
proxy_login = os.environ.get('proxy_login')
proxy_password = os.environ.get('proxy_password')

proxy = None
if proxy_enable:
    proxy = (socks.SOCKS5, proxy_ip, proxy_port, proxy_login, proxy_password)

bot = TelegramClient('bot',
                     api_id=api_id,
                     api_hash=api_hash,
                     proxy=proxy) \
    .start(bot_token=bot_token)
admin_ids = []
admin_roles = []
super_admin_ids = []
super_admin_roles = []


def welcome_screen(sender_id):
    buttons_list = []
    if sender_id in admin_ids:
        buttons_list.append(Button.text(const.USERS))
    if sender_id in super_admin_ids:
        buttons_list.append(Button.text(const.ADMINS))
        buttons_list.append(Button.text(const.RELOAD_IDS))
    return buttons_list


def verify_screen(sender_id):
    buttons_list = []
    buttons_list.append(Button.text('/start'))
    if sender_id in admin_ids:
        buttons_list.append(Button.text(const.YES))
        buttons_list.append(Button.text(const.NO))
    if sender_id in super_admin_ids:
        buttons_list.append(Button.text(const.BAN))
        buttons_list.append(Button.text(const.UNBAN))
    return buttons_list


@bot.on(events.NewMessage(pattern='/start', chats=admin_ids, from_users=admin_ids))
async def start(event):
    buttons_list = welcome_screen(event.sender_id)
    keyboard = event.client.build_reply_markup(buttons_list)

    await event.respond("Виберіть режим або відправте username користувача: ", buttons=keyboard)
    raise events.StopPropagation


@bot.on(events.NewMessage(pattern=const.USERS, chats=admin_ids, from_users=admin_ids))
async def users(event):
    buttons_list = []
    users_db = Users.select().where(Users.is_verified == False).where(Users.is_banned == False)
    buttons_list.append([Button.text('/start')])
    for user in users_db:
        buttons_list.append(
            [Button.text(f"{const.DETAIL} "
                         f"{user.user_id} "
                         f"{(str(user.first_name) + ' ' + str(user.last_name)) if user.username is None else '@' + str(user.username)} ")]
        )
    keyboard = event.client.build_reply_markup(buttons_list)

    await event.respond("Виберіть користувача для перевірки: ", buttons=keyboard)


@bot.on(events.NewMessage(pattern=const.ADMINS, chats=super_admin_ids, from_users=super_admin_ids))
async def admins(event):
    admin_list = Admins.select()
    text = ''.join([
        f"{admin.user_id} \t"
        f"{admin.last_name} {admin.first_name} \t"
        f"@{admin.username} \n"
        f"[Діалог з адміністратором](tg://user?id={str(admin.user_id)}) \n\n"
        for admin in admin_list])
    await event.respond(text)


@bot.on(events.NewMessage(pattern=const.RELOAD_IDS, chats=super_admin_ids, from_users=super_admin_ids))
async def reload(event):
    await event.respond(
        f"admin_ids {len(admin_ids)}\n" + ''.join([f"[{admin}](tg://user?id={str(admin)})\n" for admin in admin_ids]))
    # await event.respond(f"admin_roles {len(admin_roles)}\n" + ''.join([f"{admin}\n" for admin in admin_roles]))
    await event.respond(
        f"super_admin_ids {len(super_admin_ids)}\n" + ''.join(
            [f"[{admin}](tg://user?id={str(admin)})\n" for admin in super_admin_ids]))
    # await event.respond(
    #     f"super_admin_roles {len(super_admin_roles)}\n" + ''.join([f"{admin}\n" for admin in super_admin_roles]))
    load_admin()
    await event.respond(
        f"admin_ids {len(admin_ids)}\n" + ''.join([f"[{admin}](tg://user?id={str(admin)})\n" for admin in admin_ids]))
    # await event.respond(f"admin_roles {len(admin_roles)}\n" + ''.join([f"{admin}\n" for admin in admin_roles]))
    await event.respond(
        f"super_admin_ids {len(super_admin_ids)}\n" + ''.join(
            [f"[{admin}](tg://user?id={str(admin)})\n" for admin in super_admin_ids]))
    # await event.respond(
    #     f"super_admin_roles {len(super_admin_roles)}\n" + ''.join([f"{admin}\n" for admin in super_admin_roles]))
    await event.respond(f"users all in db {Users.select().count()}")
    await event.respond(f"users banned in db {Users.select().where(Users.is_banned == True).count()}")
    await event.respond(f"users verified in db {Users.select().where(Users.is_verified == True).count()}")


@bot.on(events.NewMessage(pattern='/loadchat', from_users=super_admin_ids))
async def load_chat_to_db(event):
    logging.info(f"Add new chat Chat:{event.chat_id} Sender: {event.sender_id} MessageId: {event.message.id}")
    chat = await bot.get_entity(event.chat_id)
    participants = await bot.get_participants(chat.id)
    for participant in participants:
        user_db = Users(
            user_id=participant.id,
            first_name=participant.first_name,
            last_name=participant.last_name,
            username=participant.username,
            is_verified=False,
            sys_comment=f"Loaded by bot. Timestamp: {datetime.now()}"
        )
        try:
            user_db.save(force_insert=True)
        except Exception as e:
            dbhandle.rollback()
            logging.info(f"Insert user Exception: {e}")

    group = Groups(
        group_id=chat.id,
        name=chat.title
    )
    try:
        group.save(force_insert=True)
    except Exception as e:
        dbhandle.rollback()
        logging.info(f"Insert group Exception: {e}")


@bot.on(events.NewMessage(pattern='/loadchat'))
async def remove_system_message(event):
    logging.info(f"Remove system message Chat:{event.chat_id} Sender: {event.sender_id} MessageId: {event.message.id}")
    await bot.delete_messages(event.chat_id, [event.message.id])


@bot.on(events.NewMessage(pattern=const.DETAIL, chats=admin_ids, from_users=admin_ids))
async def detail_by_id(event):
    try:
        user_id = event.text.split(' ')[1]
        user_db = Users.get(user_id=user_id)
        await event.respond(f"id: {user_db.user_id}\n"
                            f"FIO: {user_db.last_name} {user_db.first_name}\n"
                            f"Username: {'UNDEFINED' if user_db.username is None else '@' + str(user_db.username)}\n"
                            f"Verified: {user_db.is_verified}\n"
                            f"Banned: {user_db.is_banned}\n"
                            f"Comment: {user_db.comment}\n"
                            f"Sys comment: {user_db.sys_comment}\n"
                            f"[Натисніть сюди для перевірки діалогу с користувачем](tg://user?id={str(user_db.user_id)})"
                            )
        if user_db.is_verified:
            await event.respond("Користувач перевірений")
        else:
            conversation_state[event.sender_id] = user_db.user_id
            buttons_list = verify_screen(event.sender_id)
            keyboard = event.client.build_reply_markup(buttons_list)
            await event.respond("Ви знаєте чю людину? ", buttons=keyboard)
    except Exception as e:
        dbhandle.rollback()
        logging.info(f"Get user Exception: {e}")
        await event.respond(f"Каристувач {event.text} не знайдений у БД")


@bot.on(events.NewMessage(pattern='@', chats=admin_ids, from_users=admin_ids))
async def detail_by_username(event):
    try:
        user_db = Users.get(username=event.text[1:])
        await event.respond(f"id: {user_db.user_id}\n"
                            f"FIO: {user_db.last_name} {user_db.first_name}\n"
                            f"Username: {'UNDEFINED' if user_db.username is None else '@' + str(user_db.username)}\n"
                            f"Verified: {user_db.is_verified}\n"
                            f"Banned: {user_db.is_banned}\n"
                            f"Comment: {user_db.comment}\n"
                            f"[Натисніть сюди для перевірки діалогу с користувачем](tg://user?id={str(user_db.user_id)})"
                            )
        if user_db.is_verified:
            await event.respond("Користувач перевірений")
        else:
            conversation_state[event.sender_id] = user_db.user_id
            buttons_list = verify_screen(event.sender_id)
            keyboard = event.client.build_reply_markup(buttons_list)
            await event.respond("Ви знаєте чю людину? ", buttons=keyboard)
    except Exception as e:
        dbhandle.rollback()
        logging.info(f"Get user Exception: {e}")
        await event.respond(f"Каристувач {event.text} не знайдений у БД")


@bot.on(events.NewMessage(pattern=const.YES, chats=admin_ids, from_users=admin_ids))
async def verified_user(event):
    upd = Users.update(is_verified=True, comment=f"Verified by {event.sender_id}, Date: {datetime.now()}") \
        .where(Users.user_id == conversation_state[event.sender_id])
    upd.execute()
    del conversation_state[event.sender_id]

    buttons_list = welcome_screen(event.sender_id)
    keyboard = event.client.build_reply_markup(buttons_list)

    await event.respond("Виберіть режим або відправте username користувача: ", buttons=keyboard)


@bot.on(events.NewMessage(pattern=const.NO, chats=admin_ids, from_users=admin_ids))
async def not_verified_user(event):
    upd = Users.update(is_verified=True, comment=f"UNDEFINED CONTACT. By {event.sender_id}, Date: {datetime.now()}") \
        .where(Users.user_id == conversation_state[event.sender_id])
    upd.execute()
    del conversation_state[event.sender_id]

    buttons_list = welcome_screen(event.sender_id)
    keyboard = event.client.build_reply_markup(buttons_list)

    await event.respond("Виберіть режим або відправте username користувача: ", buttons=keyboard)


@bot.on(events.NewMessage(pattern=const.BAN, chats=super_admin_ids, from_users=super_admin_ids))
async def ban_user(event):
    user_id = conversation_state[event.sender_id]
    groups_db = Groups.select()
    for group in groups_db:
        grpid = group.group_id
        await bot.edit_permissions(grpid, user_id, view_messages=False)
        await event.respond(f"User {user_id} banned in chat {grpid} {group.name}")
    user_db = Users.get(user_id=user_id)
    user_db.is_banned = True
    user_db.comment = f"BANNED by {event.sender_id}, Date: {datetime.now()}"
    user_db.save()

    buttons_list = welcome_screen(event.sender_id)
    keyboard = event.client.build_reply_markup(buttons_list)

    await event.respond("Виберіть режим або відправте username користувача: ", buttons=keyboard)


@bot.on(events.NewMessage(pattern=const.UNBAN, chats=super_admin_ids, from_users=super_admin_ids))
async def unban_user(event):
    user_id = conversation_state[event.sender_id]
    groups_db = Groups.select()
    for group in groups_db:
        grpid = group.group_id
        await bot.edit_permissions(grpid, user_id, view_messages=True)
        await event.respond(f"User {user_id} unbanned in chat {grpid} {group.name}")
    user_db = Users.get(user_id=user_id)
    user_db.is_banned = True
    user_db.comment = f"Unbanned by {event.sender_id}, Date: {datetime.now()}"
    user_db.save()

    buttons_list = welcome_screen(event.sender_id)
    keyboard = event.client.build_reply_markup(buttons_list)

    await event.respond("Виберіть режим або відправте username користувача: ", buttons=keyboard)


@bot.on(events.NewMessage(pattern='/start'))
async def test_get_ids(event):
    logging.info(f"Sender: {event.sender_id}")


def load_admin():
    admin_list = Admins.select()
    temp_admin_ids = []
    temp_admin_roles = []
    temp_super_admin_ids = []
    temp_super_admin_roles = []
    for admin in admin_list:
        temp_admin_ids.append(admin.user_id)
        temp_admin_roles.append({"user_id": admin.user_id, "role": admin.role})
        if admin.role == 'superadmin':
            temp_super_admin_ids.append(admin.user_id)
            temp_super_admin_roles.append({"user_id": admin.user_id, "role": admin.role})
    global admin_ids
    admin_ids.clear()
    admin_ids += temp_admin_ids
    global admin_roles
    admin_roles.clear()
    admin_roles += temp_admin_roles
    global super_admin_ids
    super_admin_ids.clear()
    super_admin_ids += temp_super_admin_ids
    global super_admin_roles
    super_admin_roles.clear()
    super_admin_roles += temp_super_admin_roles


def main():
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
    load_admin()
    bot.run_until_disconnected()


if __name__ == '__main__':
    main()
